package program;


//*Prints all pair of integer values from given array whose sum is is equal to given number. * complexity of this solution is O(n^2)


public class ArrayProblem1 {
	public static void main(String[] args) {
		
		int sum=6;
		
		int[] array={2,4,3,5,6,-2,4,7,8,9};
		
		int len=array.length;
		
		for (int i = 0; i < array.length; i++) {
			int first=array[i];
			{
				for (int j = i+1; j < array.length; j++) {
					
				int second=array[j];
					
				if((first+second)==sum)
				{
					System.out.printf("(%d, %d) %n", first, second);
				}
				}
			}
			
		}
		
		
		
	}
	
	
	
	//OUTPUT LIKE : (2, 4) ,(2, 4),(-2, 8) 
	
		
	
	
	
	
	

}
