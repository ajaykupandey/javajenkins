package program;

public class MathsPowerFunction {
	
	//* Evaluating x^n with linear complexity solution.(n=positive number)ie.o(n)
	
	public static  void evaluatepowerN( int base,int power)
	{
		int value=1;
		for (int i =1; i<=power;i++) {		
			value=value* base;	
		}
		value=value*value;
		System.out.println(value);
		
		
}	
	//* Evaluating x^n with minimum possible complexity using iteration not recursion.(n=positive number)ie. 0(n/2)
	public static  void evaluatepowerN1( int base,int power)
	{
		int value=1;
		// Test case 1 *********When the value of power is even************
		if(power%2==0)
		{
		power=power/2;
         for (int i =1; i<=power;i++) {
			
			value=value* base;			
		}
         value=value*value;
		
		System.out.println(value);
		
		
	}
		// Test case 2 **************when the value of power is odd.**************
		else if (!((power%2)==0)) {
		power=(int) Math.floor(power/2);
		
			for (int i =1; i<=power;i++) {
			value=value* base;		
		}
		value=value*value;
		value=value*base;
		System.out.println(value);	
		}
		// Test Case 3 *****************when the value of power is zero****************
		else if (power==0) {
	   System.out.println(value);
			
		}
	}
}

//**Driver class*****

 class Test{
	 
	 public static void main(String[] args) {
		 
		 MathsPowerFunction.evaluatepowerN1(21, 21);
		 MathsPowerFunction.evaluatepowerN1(6, 8);
		 MathsPowerFunction.evaluatepowerN1(9, 0);
		
	}
 }
	 
	 

	



	
	
		
	
	
	
	

	


		
		
	
	
	
