package program;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateCharacterinString {
	public static void main(String[] args) {
		
		String test="ajay kumar pandey";
		String test1=test.replaceAll(" ", "");
		Map<Character, Integer> charcountmap=new HashMap<Character,Integer>();
		
		char[] Str=test1.toCharArray();
		
		for (char c : Str) {
			
			if(charcountmap.containsKey(c))
			{

             charcountmap.put(c, charcountmap.get(c)+1);
			
		}
			else {
				
				charcountmap.put(c, 1);
				
			}
			//getting a set containing all keys of the count map
			
			Set<Character>  charsinstring=charcountmap.keySet();
			 for (Character ch : charsinstring) {
				 
				 if(charcountmap.get(ch)>1)
				 {
				System.out.println("count character :"+" "+ ch  + "  " + charcountmap.get(ch));
			}
		
		
		
				
				
	}
}
}
}