package program;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArrayListSearchingandSorting {

	
	public static void main(String[] args) {
		
		ArrayList<Integer> Marks=new ArrayList<Integer>();
		
		Marks.add(76);
		Marks.add(87);
		Marks.add(85);
		Marks.add(37);
		Marks.add(98);
		Marks.add(99);
		
		System.out.println(Marks);
		
		Iterator it=Marks.iterator();
		
		while (it.hasNext()) {
			int score= (int) it.next();
			System.out.println(score);
			
		}
		
		//natural ascending order sorting
		
		Collections.sort(Marks);
		
		System.out.println(Marks);
		
		
		//Binary search on sorted list.
		
		int searchindex=Collections.binarySearch(Marks, 85);
		if(searchindex>=0)
		{
			
			System.out.println("Element found at index:"+ " " + searchindex);
		}
		
		else
		{
			System.out.println("Element not found");
		}
	
		}
		
		
	}

