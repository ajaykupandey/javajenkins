package program;

import java.util.HashMap;
import java.util.Set;

public class DuplicateWordsinString {
	
	static void duplicatewords(String inputstring)
	{
		
	 String[] words=inputstring.split(" ");
	 
	 HashMap<String, Integer> Wordcount=new HashMap<String,Integer>();
	 
	 for (String word : words) {
		 
		 if(Wordcount.containsKey(word.toLowerCase()))
		 {
			 
			 Wordcount.put(word.toLowerCase(), Wordcount.get(word.toLowerCase()) +1);
		 }
		 
		 else
		 {
			 Wordcount.put(word.toLowerCase(), 1);
		 }
		
	}
		
		
		Set<String> WordsinString=Wordcount.keySet();
		
		for (String word : WordsinString) {
			
			if(Wordcount.get(word)>1)
			{
				System.out.println(word + " :" + Wordcount.get(word));
			}
			
		}
		
		
	
		
		
	}
}


class Test1
{
	public static void main(String[] args) {
		
		DuplicateWordsinString.duplicatewords("this is not good is");
		DuplicateWordsinString.duplicatewords("Ajay pandey is rarely  called as Ajay pandey");
		
	}
}
