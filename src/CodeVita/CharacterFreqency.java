package codestring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CharacterFreqency {
	
public static void printduplicatecharacters(String word)
	
	{
		
		String wordTest=word.replaceAll(" ", "");
		char[] characters=wordTest.toCharArray();
		
		Map<Character, Integer> charmap=new HashMap<Character, Integer>();
		for(Character ch:characters){
			if(charmap.containsKey(ch))
			{
				charmap.put(ch, charmap.get(ch)+1);
			}
			else
			{
				charmap.put(ch, 1);
			}
		}
		//Iterate through Hashmap to print duplicate characters of a string
		
		Set<Map.Entry<Character, Integer>> entryset=charmap.entrySet();
		System.out.println("List of duplicate characters in the string :");
		for(Map.Entry<Character ,Integer> entry:entryset)
		{
		//if(entry.getValue()>1)
		//{
			System.out.println(entry.getKey()+ "  "  + entry.getValue());
		}
		}
	}
			

	
	
